import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager


# I am using python unittest for asserting cases.
# I am using Pytest to generate allure report.

class BaseTest(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('no-sandbox')
        options.add_argument('--disable-gpu')
        options.add_argument('window-size=0x0')
        #self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)


        self.driver = webdriver.Remote(
            command_executor='http://selenium__standalone-chrome:4444/wd/hub',
            desired_capabilities = {'browserName': 'chrome'},
            options=options
        )


        self.driver.maximize_window()
        self.driver.get("https://www.ebay.com")

    def tearDown(self):
        self.driver.close()
