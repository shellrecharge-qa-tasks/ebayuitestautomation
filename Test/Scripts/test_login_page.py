import allure
from PageObject.Pages.main_page import MainPage
from Test.Scripts.base_test import BaseTest


# I am using python unittest for asserting cases.
# In this module, there should be test cases.

class TestLoginPage(BaseTest):

    @allure.step("Check Page Load")
    def test_page_load(self):
        page = MainPage(self.driver)
        self.assertTrue(page.check_page_loaded())

    @allure.step("Check Searchbox")
    def test_search_item(self):
        page = MainPage(self.driver)
        search_result = page.search_item("iPhone")
        self.assertIn("iPhone", search_result)

    @allure.step("Check Register Button")
    def test_register_button(self):
        page = MainPage(self.driver)
        register_page = page.click_register_button()
        self.assertIn("/signup.ebay", register_page.get_url())

    @allure.step("Check Login Button")
    def test_sign_in_button(self):
        page = MainPage(self.driver)
        login_page = page.click_sign_in_button()
        self.assertIn("signin.ebay", login_page.get_url())

    @allure.step("Check login with valid user")
    def test_sign_in_with_valid_user(self):
        main_page = MainPage(self.driver)
        login_page = main_page.click_sign_in_button()
        result = login_page.login_with_valid_user("valid_user")
        self.assertIn("ebay", result.get_url())

    @allure.step("Check login with invalid user")
    def test_sign_in_with_in_valid_user(self):
        main_page = MainPage(self.driver)
        login_page = main_page.click_sign_in_button()
        # result = login_page.login_with_in_valid_user("invalid_user")
        # self.assertIn("Oops, that's not a match.", result)
