import allure

from PageObject.Pages.adding_product_page import AddingProductPage
from PageObject.Pages.main_page import MainPage
from Test.Scripts.base_test import BaseTest

# I am using python unittest for asserting cases.
# In this module, there should be test cases.

class TestAddingProductPage(BaseTest):

    @allure.step("Check Searchbox")
    def test_search_item(self):
        page = MainPage(self.driver)
        search_result = page.search_item("iPhone")
        self.assertIn("iPhone", search_result)

    @allure.step("Random category selection")
    def test_select_random_categories(self):
        page = MainPage(self.driver)
        search_result = page.search_item("iPhone")
        self.assertIn("iPhone", search_result)
        listPage = MainPage(self.driver)
        listPage.select_random_categories()

    @allure.step("Select Product")
    def test_adding_product_cart(self):
        page = MainPage(self.driver)
        search_result = page.search_item("iPhone")
        self.assertIn("iPhone", search_result)
        page.click_first_item()
        addProductPage = AddingProductPage(self.driver)
        addProductPage.check_page_loaded()
        addProductPage.click_add_to_card_button()
        addProductPage.check_shopping_card_page_loaded()
        button_text = addProductPage.click_go_to_checkout_button()
        self.assertIn("Confirm and pay", button_text)
