import random
import time

from selenium.webdriver.common.keys import Keys
from PageObject.Pages.base_page import BasePage
from PageObject.Pages.login_page import LoginPage
from PageObject.Pages.register_page import RegisterPage
from PageObject.locators import *


# Page objects are written in this module.
# Depends on the page functionality we can have more functions for new classes




class MainPage(BasePage):
    def __init__(self, driver):
        self.locator = MainPageLocators
        super().__init__(driver)  # Python3 version

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.LOGO) else False

    def search_item(self, item):
        self.find_element(*self.locator.SEARCH).send_keys(item)
        self.find_element(*self.locator.SEARCH).send_keys(Keys.ENTER)
        return self.find_element(*self.locator.SEARCH_LIST).text

    def click_first_item(self):
        self.find_element(*self.locator.SEARCH_LIST_FIRST).click()
        return  self.switch_chrome_driver(1)



    def select_random_categories(self):
        # refetch the checkboxes after the sorting
        select_random_batches = self.find_elements(*self.locator.NEWTWORK_CHECKBOX_LIST)
        # Selects a random element from the list of elements
        element = random.choice(select_random_batches)
        self.element_to_be_selected(*self.locator.NEWTWORK_CHECKBOX_LIST_FIRST)
        element.click();

    def click_register_button(self):
        self.find_element(*self.locator.REGISTER).click()
        return RegisterPage(self.driver)

    def click_sign_in_button(self):
        self.find_element(*self.locator.LOGIN).click()
        return LoginPage(self.driver)
