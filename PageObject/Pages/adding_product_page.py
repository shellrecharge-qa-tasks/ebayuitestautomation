import random
import time

from selenium.webdriver.common.keys import Keys
from PageObject.Pages.base_page import BasePage
from PageObject.Pages.login_page import LoginPage
from PageObject.Pages.register_page import RegisterPage
from PageObject.locators import *


# Page objects are written in this module.
# Depends on the page functionality we can have more functions for new classes

class AddingProductPage(BasePage):
    def __init__(self, driver):
        self.locator = AddingProductPageLocators()
        super().__init__(driver)  # Python3 version

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.ADD_TO_CART) else False

    def click_add_to_card_button(self):
        self.wait_element_presence(*self.locator.ADD_TO_CART)
        self.find_element(*self.locator.ADD_TO_CART).click()
        self.wait_element_presence(*self.locator.GO_TO_CHECKOUT)
        return self.find_element(*self.locator.GO_TO_CHECKOUT).text

    def check_shopping_card_page_loaded(self):
        return True if self.find_element(*self.locator.GO_TO_CHECKOUT) else False

    def click_go_to_checkout_button(self):
        self.find_element(*self.locator.GO_TO_CHECKOUT).click()
        self.wait_element_presence(*self.locator.CONFIRM_AND_PAY)
        return self.find_element(*self.locator.CONFIRM_AND_PAY).text
