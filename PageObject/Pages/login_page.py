from PageObject.Pages.home_page import HomePage
from PageObject.locators import *
from PageObject.Pages.base_page import BasePage
from Utils import users


class LoginPage(BasePage):
    def __init__(self, driver):
        self.locator = LoginPageLocators
        super(LoginPage, self).__init__(driver)  # Python2 version

    def enter_email(self, email):
        self.find_element(*self.locator.EMAIL).send_keys(email)

    def enter_password(self, password):
        self.find_element(*self.locator.PASSWORD).send_keys(password)

    def click_continue_button(self):
        self.find_element(*self.locator.CONTINUE).click()

    def click_signin_button(self):
        self.find_element(*self.locator.SIGNIN).click()

    def login(self, user):
        user = users.get_user(user)
        print(user)
        self.enter_email(user["email"])
        self.click_continue_button()
        self.wait_element_clickable(*self.locator.PASSWORD)
        self.enter_password(user["password"])
        self.wait_element_clickable(*self.locator.SIGNIN)
        self.click_signin_button()

    def login_with_valid_user(self, user):
        self.login(user)
        return HomePage(self.driver)

    def login_with_in_valid_user(self, user):
        self.login(user)
        return self.find_element(*self.locator.ERROR_MESSAGE).text
