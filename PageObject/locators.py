from selenium.webdriver.common.by import By

class MainPageLocators(object):
    LOGO = (By.ID, 'gh-logo')
    ACCOUNT = (By.ID, 'nav-link-accountList')
    REGISTER = (By.XPATH, '//*[@id="gh-ug-flex"]/a')
    LOGIN = (By.XPATH, '//*[@id="gh-ug"]/a')
    SEARCH = (By.ID, 'gh-ac')
    SEARCH_LIST = (By.XPATH, '//*[@id="srp-river-results"]/ul/li')
    SEARCH_LIST_FIRST = (By.XPATH, '//*[@id="srp-river-results"]/ul/li[2]/div/div[2]/a')
    NEWTWORK_CHECKBOX_LIST = (By.XPATH, '//*[@id="x-refine__group_1__0"]//input')
    NEWTWORK_CHECKBOX_LIST_FIRST = (By.XPATH, '//*[@id="x-refine__group_1__0"]/ul/li[1]//input[1]')

class LoginPageLocators(object):
    EMAIL = (By.ID, 'userid')
    PASSWORD = (By.ID, 'pass')
    SUBMIT = (By.ID, 'signInSubmit-input')
    CONTINUE = (By.ID, 'signin-continue-btn')
    SIGNIN = (By.ID, 'sgnBt')
    ERROR_MESSAGE = (By.ID, 'errormsg')

class AddingProductPageLocators(object):
    ADD_TO_CART = (By.ID, 'isCartBtn_btn')
    GO_TO_CHECKOUT = (By.XPATH, '//*[@id="mainContent"]//button')
    CONFIRM_AND_PAY = (By.XPATH, '//*[@id="page-form"]/div/button')



