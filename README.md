#  Project Structure
* Ebay e-commerce website was used for UI test automation. --> [Link Reqres](https://reqres.in/)
* Programming Language: JAVA [JavaSE - 1.8]
* IDE: PyCharm 
* OS : Windows 10
* Python unit test project created;


##  Page Object Model (POM) requires a feature file to invoke the step definitions:

* Page Object Model (POM) model was used for UI tests. BasePageClass has been created. And other page classes that can use the attributes and methods of this BasePageClass have been extended.
* Page-object model is a pattern that you can apply it to develop efficient automation framework. With page-model, it is possible to minimise maintenance cost. Basically page-object means that your every page is inherited from a base class which includes basic functionalities for every pages.
* Two test frameworks were used. Unittest was used for test assertions and run, and pytest was used to generate allure report.

# Page Object Model in Selenium & Python:

```
app
└── PageObject
    └── Pages
    │    │	└── base_page.py
	│	 │	└── login_page.py
	│	 │	└── home_page.py
	│	 │	└── adding_product_page.py
	│	 │	└── ""
	│	 └──locators.py
	│
    └──  Test
    │    │	└── Scripts
	│	 │			└──base_test.py
	│	 │			└──test_adding_product_page.py
	│	 │			└──test_login_page.py
	│	 │
	│	 └── TestSuite
	│				└── home_page.py
	│				└── ebay_testsuite_testrunner.py
	│
	│
    └──	Utils
        	└── users.py
```

### Install dependencies

```
pip install -r requirements.txt
```

### Run Test Local

```
python3 -m pytest Test/TestSuite/ebay_testsuite_testrunner.py --alluredir json_reports

OR

python -m unittest
```


### Test Execution Cloud & Gitlab CI

* You can see the configuration details in the `ebayuitestautomation
.gitlab-ci.yml` file path
* The application is dockerized and Used `python:3.9-alpine3.15` image and `standalone-chrome:latest service`
* The tests were configured with gitlab ci. Auto runable condition
* [You can find the CI/CD Piple here](https://gitlab.com/shellrecharge-qa-tasks/ebayuitestautomation/-/pipelines)

![Pipeline flow](https://gitlab.com/shellrecharge-qa-tasks/ebayuitestautomation/-/raw/main/images/pipline-ui.PNG)


### Test Report
Allure report tool is used for reporting. Configured in Gitlab Ci. After the tests are run, the html report is automatically generated for the generated .json files.

* [Test report](https://shellrecharge-qa-tasks.gitlab.io/-/reqresapitestautomation/-/jobs/2750380009/artifacts/public/index.html)

![Test Report](https://gitlab.com/shellrecharge-qa-tasks/reqresapitestautomation/-/raw/main/images/allure-api.PNG)


### Tools / libraries used :
* Python
* Pytest
* allure-pytest
* chromedriver
* selenium

### [PROBLEM] During the tests, the following problems were seen.

